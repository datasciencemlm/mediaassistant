#testing conducted with pytest
import pytest
from media_assistant import *
import eurosport
from lex_tools import parse_date

def test_querygenerator():
    intent_req = {'currentIntent':{'slots':{}}}
    assert eurosport.generate_metric_query(metric="Spend",intent_request=intent_req) == "SELECT sum(Spend) as Spend FROM prod_channels;"

def test_querygenerator_2():
    intent_req = {'currentIntent':{'slots':{'Market':'DE'}}}
    assert eurosport.generate_metric_query(metric="Spend",intent_request=intent_req) == "SELECT sum(Spend) as Spend FROM prod_channels WHERE Country in ('DE');"

def test_querygenerator_unconverntional_name():
    intent_req = {'currentIntent':{'slots':{'Market':'Germany','MarketTwo':'Italy','Media':'Paid Search'}}}
    assert eurosport.generate_metric_query(metric="Spend",intent_request=intent_req) == "SELECT sum(Spend) as Spend FROM prod_channels WHERE Channel in ('Paid Search') AND Country in ('DE','IT');"

def test_queryresult():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Metric":"Subscriptions","Market": "Nordics","MarketTwo": "DE","MarketThree":"NL","Campaign": "WintersportsOlympics","CampaignTwo":None,"Date":None}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs FROM prod_channels WHERE COUNTRY IN ('DE','DK','NO','SE','FI','NL') AND Campaign in ('WintersportsOlympics');"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(int(query_result['Subs'])) + ' subscriptions.'
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result

def test_null_query():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Metric":"Subscriptions","Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Date":None}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs FROM prod_channels;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(int(query_result['Subs'])) + ' subscriptions.'
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result

def test_query_with_breakdown_dimensions():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": "DE","MarketTwo": "IT","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":None,"Metric":"Subscriptions"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    result = dispatch(intent_req)

    assert isinstance(result['dialogAction']['message']['content'],str)

def test_date_week():
    assert parse_date("2018-W01",past=True) == {"start":"2017-12-31","end":"2018-01-06"}

def test_date_single_day():
    assert parse_date("2018-01-01",past=True) == {"start":"2018-01-01","end":"2018-01-01"}

def test_month():
    assert parse_date("2018-01",past=True) == {"start":"2018-01-01","end":"2018-01-31"} and parse_date("2018-02",past=True) == {"start":"2018-02-01","end":"2018-02-28"}

def test_year():
    assert parse_date("2018",past=True) == {"start":"2018-01-01","end":"2018-12-31"} and parse_date("2017",past=True) == {"start":"2017-01-01","end":"2017-12-31"}

def test_year_result():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": "DE","MarketTwo": "IT","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2017","Metric":"Subscriptions"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs FROM prod_channels WHERE COUNTRY IN ('DE','IT') AND Date >= '2017-01-01' and Date <= '2017-12-31'"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(int(query_result['Subs'])) + ' subscriptions.'
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result

def test_bare_month_name():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2019-01","Metric":"Subscriptions"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs FROM prod_channels WHERE Date >= '2018-01-01' and Date <= '2018-01-31';"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(int(query_result['Subs'])) + ' subscriptions.'
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result

def test_bare_month_name_spend():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2017-10","Metric":"Spend"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend) as Spend FROM prod_channels WHERE Date >= '2017-10-01' and Date <= '2017-10-31';"
            cursor.execute(query)
            query_result = cursor.fetchone()
            print query_result
            query_result = str(round(query_result['Spend'],2)) + ' Euro.'
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result


def test_sac_dimensions():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Country","Date":None,"Metric":"SAC"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    result = dispatch(intent_req)
    assert isinstance(result['dialogAction']['message']['content'],str)

def test_weekend_spend():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2018-W06-WE","Metric":"Spend"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend) as Spend FROM prod_channels WHERE Date >= '2018-02-10' and Date <= '2018-02-11';"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(round(query_result['Spend'],2)) + ' Euro.'
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result

def test_find_best_Subs():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": None,"Metric":"Subscriptions","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":"2018-W06-WE","Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs , Campaign FROM prod_channels WHERE Date >= '2018-02-10' and Date <= '2018-02-11' GROUP BY Campaign ORDER BY Subs DESC LIMIT 1;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "Campaign: WintersportsOlympics, Subscriptions: " + str(int(query_result['Subs'])) + "."
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result


def test_find_best_SAC_best():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": None,"Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":None,"Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC , Campaign FROM prod_channels WHERE Spend > 0 and Conversions > 0 GROUP BY Campaign ORDER BY SAC ASC LIMIT 1;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: Exact"  + "."
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result

def test_find_best_SAC_worst():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": None,"Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":None,"Extreme":"worst"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC , Campaign FROM prod_channels WHERE Spend > 0 and Conversions > 0 GROUP BY Campaign ORDER BY SAC DESC LIMIT 1;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEurolegue"  + "."
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result

def test_find_best_SAC_worst_week():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": None,"Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":"2018-W6","Extreme":"worst"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC , Campaign FROM prod_channels WHERE Spend > 0 and Conversions > 0 and Date >= '2018-02-04' and Date <= '2018-02-10' GROUP BY Campaign ORDER BY SAC DESC LIMIT 1;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == query_result

def test_SAC_DE_last_week():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": "DE","Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2018-W6","Extreme":"worst"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Country = 'DE' and Date >= '2018-02-04' and Date <= '2018-02-10';"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = dispatch(intent_req)
    assert isinstance(result['dialogAction']['message']['content'],str)


def test_SAC_DE_last_week():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": "DE","Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Channel","Date":"2018-W07-WE","Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Country = 'DE' and Date >= '2018-02-04' and Date <= '2018-02-10';"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    try:
        result = dispatch(intent_req)
        status = "succeded"
    except:
        status = "failed"
    assert status == "failed"

def test_CTR_DE_last_week_best():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": "DE","Metric":"CTR","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Channel","Date":"2018-W06-WE","Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Country = 'DE' and Date >= '2018-02-04' and Date <= '2018-02-10';"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == 'Channel: Paid Search, CTR: 10.27.'

def test_CVR_DE_last_week_best():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": "DE","Metric":"CVR","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Channel","Date":"2018-W06-WE","Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Country = 'DE' and Date >= '2018-02-04' and Date <= '2018-02-10';"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == 'CVR: 0.08, Channel: Paid Search.'


def test_greeting():
    intent_req = {"currentIntent": {"name": "Greeting","slots": {"statusCheck":"How are you"}}}
    result = dispatch(intent_req)
    assert result['dialogAction']['message']['content'] == 'Hey, good thanks. What would you like to know?'

def test_greeting():
    intent_req = {"currentIntent": {"name": "Greeting","slots": {"statusCheck":"How are you"}}}
    result = dispatch(intent_req)
    assert "?" in result['dialogAction']['message']['content']


def test_greeting_no_status_check():
    intent_req = {"currentIntent": {"name": "Greeting","slots": {"statusCheck":None}}}
    result = dispatch(intent_req)
    assert "?" in result['dialogAction']['message']['content']


def test_CPC():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"Metric":"CPC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":None,"Extreme":None}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Clicks) as CPC FROM prod_channels;"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "CTR: " + str(round(query_result['CPC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = dispatch(intent_req)
    assert "Euro per click" not in result['dialogAction']['message']['content']
