"""
This sample demonstrates an implementation of the Lex Code Hook Interface
in order to serve a sample bot which manages reservations for hotel rooms and car rentals.
Bot, Intent, and Slot models which are compatible with this sample can be found in the Lex Console
as part of the 'BookTrip' template.

For instructions on how to set up and test this bot, as well as additional samples,
visit the Lex Getting Started documentation http://docs.aws.amazon.com/lex/latest/dg/getting-started.html.
"""

import json

import logging
import pymysql.cursors
import random

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

# --- Client package

from eurosport import *

""" --- Functions that control the bot's behavior --- """

def query_db(db,query):
    try:
        with db.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
            db.close()
    except:
        result = "Sorry, I'm having trouble reaching the datasource"

    return result

def return_text(text):
    return {"dialogAction": {"type": "Close","fulfillmentState": "Fulfilled","message": {"contentType": "PlainText","content": text}}}

def connect_to_db(client):
    if client == 9999:
        return pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                     user='redash',
                                     password='Explode-Cape-Reproduction-Ground-Talk-5',
                                     db='client_9999',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)

# --- Intents ---

def dispatch(intent_request):
    """
    Called when the user specifies an intent for this bot.
    """

    #logger.debug('dispatch userId={}, intentName={}'.format(intent_request['userId'], intent_request['currentIntent']['name']))

    intent_name = intent_request['currentIntent']['name']

    # Dispatch to your bot's intent handlers
    if intent_name == "SingleMetric":
        metric_name = intent_request['currentIntent']['slots']['Metric']
        if metric_name == 'Spend':
            #initialise connection
            db =connect_to_db(9999)
            #fetch the data
            result = query_db(db=db,query=generate_metric_query(metric="Spend",intent_request=intent_request))
            print "result:",result
            #if there's only one result (i.e. no breakdowns)
            if len(result) == 1:
                result = result[0]['Spend']
                try:
                    result = round(result,2)
                except:
                    result = 0.00
                result = str(result) + " Euro."
                response = return_text(result)
                print "response:",response
                return response
            else:
                total = 0
                text = "The breakdown is as follows: "
                for row in result:
                    for key in row.keys():
                        if key != 'Spend':
                            text += str(row[key]) + " "
                    spend = round(row['Spend'])
                    text += "= " + str(spend) + " Euro , "
                    total += spend
                text += "totalling " + str(total) + " Euro."
                print "final text:",text
                response = return_text(text)
                print "response:",response
                return response

        elif metric_name == 'Subscriptions':
            #initialise connection
            db =connect_to_db(9999)
            #fetch result
            result = query_db(db=db,query=generate_metric_query(metric="Subscriptions",intent_request=intent_request))
            print "result:",result
            #if there's only one result (i.e. no breakdowns)
            if len(result) == 1:
                result = result[0]['Subscriptions']
                result = int(result)
                result = str(result) + " subscriptions."
                response = return_text(result)
                print "response:",response
                return response
            #otherwise
            else:
                total = 0
                text = "The breakdown is as follows: "
                for row in result:
                    for key in row.keys():
                        if key != 'Subscriptions':
                            if row[key] != "":
                                text += str(row[key]) + " "
                            else:
                                text += "NULL "
                    try:
                        subs = int(row['Subscriptions'])
                    except:
                        subs = 0
                    text += "= " + str(subs) + ", "
                    total += subs
                text += "totalling " + str(total) + " subscriptions."
                response = return_text(text)
                print "response:",response
                return response

        elif metric_name == 'SAC':
            units = "Euro per sub"
            #initialise connection
            db =connect_to_db(9999)
            #fetch the data
            result = query_db(db=db,query=generate_metric_query(metric=metric_name,intent_request=intent_request))
            #if there's only one result
            if len(result)==1:
                result = result[0][metric_name]
                result = round(result,2)
                result = str(result) + " " + units + "."
                response = return_text(result)
            #otherwise
            else:
                text = "The breakdown is as follows: "
                for row in result:
                    for counter,key in enumerate(row.keys()):
                        if key != metric_name:
                            if row[key] != "":
                                text += str(row[key]) + " "
                            else:
                                text += "NULL "

                    try:
                        value = round(row[metric_name],2)
                    except:
                        value = 0

                    if counter != len(row.keys()):
                        text += "= " + str(value) + " " + units + ","
                    else:
                        text += "= " + str(value) ++ " " + units + "."
                response = return_text(text)

            print "response:",response
            return response

        elif metric_name == 'CPC':
            units = "Euro per click"
            #initialise db connection
            db =connect_to_db(9999)
            #fetch result
            result = query_db(db=db,query=generate_metric_query(metric=metric_name,intent_request=intent_request))
            print result
            #if there's only one result
            if len(result)== 1:
                result = result[0][metric_name]
                result = round(result,2)
                result = str(result) + " " + units + "."
                response = return_text(result)
            #otherwise
            else:

                text = "The breakdown is as follows: "
                for row in result:
                    for counter,key in enumerate(row.keys()):
                        if key != metric_name:
                            if row[key] != "":
                                text += str(row[key]) + " "
                            else:
                                text += "NULL "

                    try:
                        value = round(row[metric_name],2)
                    except:
                        value = 0

                    if counter != len(row.keys()):
                        text += "= " + str(value) + " " + units + ","
                    else:
                        text += "= " + str(value) ++ " " + units + "."
                response = return_text(text)

            print "response:",response
            return response

        elif metric_name == 'CTR':
            units = "%"
            #initialise db connection
            db =connect_to_db(9999)
            #fetch result
            result = query_db(db=db,query=generate_metric_query(metric=metric_name,intent_request=intent_request))
            #if there's only one result
            if len(result)== 1:
                result = result[0][metric_name]
                result = round(result,2)
                result = str(result) + " " + units + "."
                response = return_text(result)
            #otherwise
            else:

                text = "The breakdown is as follows: "
                for row in result:
                    for counter,key in enumerate(row.keys()):
                        if key != metric_name:
                            if row[key] != "":
                                text += str(row[key]) + " "
                            else:
                                text += "NULL "

                    try:
                        value = round(row[metric_name],2)
                    except:
                        value = 0

                    if counter != len(row.keys()):
                        text += "= " + str(value) + " " + units + ","
                    else:
                        text += "= " + str(value) ++ " " + units + "."
                response = return_text(text)

            print "response:",response
            return response

        elif metric_name == 'CVR':
                units = "%"
                #initialise db connection
                db =connect_to_db(9999)
                #fetch result
                result = query_db(db=db,query=generate_metric_query(metric=metric_name,intent_request=intent_request))
                #if there's only one result
                if len(result)==1:
                    result = result[0][metric_name]
                    result = round(result,2)
                    result = str(result) + " " + units + "."
                    response = return_text(result)
                #otherwise
                else:

                    text = "The breakdown is as follows: "
                    for row in result:
                        for counter,key in enumerate(row.keys()):
                            if key != metric_name:
                                if row[key] != "":
                                    text += str(row[key]) + " "
                                else:
                                    text += "NULL "

                        try:
                            value = round(row[metric_name],2)
                        except:
                            value = 0

                        if counter != len(row.keys()):
                            text += "= " + str(value) + " " + units + ","
                        else:
                            text += "= " + str(value) ++ " " + units + "."
                    response = return_text(text)

                print "response:",response
                return response

    elif intent_name == 'FindBest':

        metric = intent_request['currentIntent']['slots']['Metric']

        db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                     user='redash',
                                     password='Explode-Cape-Reproduction-Ground-Talk-5',
                                     db='client_9999',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)

        result = query_db(db=db,query=comparison_query(intent_request))

        result = result[0]
        print result
        response = ""
        for counter,key in enumerate(result.keys()):
            #parse the keys and values
            try:
                if key == metric:
                    if metric == "Subscriptions":
                        response += key + ": " + str(int(result[key]))
                    else:
                        response += key + ": " + str(round(result[key],2))
                else:
                    response += key + ": " + str(result[key])
            except:
                response += key + ": " + " None"

            #period or a comma?
            if counter + 1 != len(result.keys()):
                response += ", "
            else:
                response += "."

        response = return_text(response)
        return response

    elif intent_name == 'Greeting':
            emo_mode = False

            if emo_mode == True:
                if intent_request['currentIntent']['slots']['statusCheck'] != None:
                     responses = ['Hey, been better. Anyway, what would you like to know?',"Another day another dollar, right?. What would you like to know?","Yeah, I'm alright *sigh*, what would you like to know?"]
                else:
                    responses = ["Hey, what would you like to know?","*deep breaths* Hey, how can I help this time?","Another day another dollar. What would you like to know?"]
            elif emo_mode == False:
                if intent_request['currentIntent']['slots']['statusCheck'] != None:
                     responses = ["Hey, I'm good! what would you like to know?","Hey boss! Fine and dandy. What would you like to know?"]
                else:
                    responses = ["Hey, what would you like to know?","Hey, how can I help?","G'day mate. What would you like to know?"]

            response = return_text(random.choice(responses))
            return response


    #raise Exception('Intent with name ' + intent_name + ' not supported')


# --- Main handler ---


def lambda_handler(event, context):
    """
    Route the incoming request based on intent.
    The JSON body of the request is provided in the event slot.
    """


    #logger.debug('event.bot.name={}'.format(event['bot']['name']))
    try:
        return dispatch(event)
    except:
        return return_text("Oops, I had trouble understanding some of the question. Could you please ask it again a little differently? For example, instead of saying 'over the weekend', you could say 'last weekend'.")
