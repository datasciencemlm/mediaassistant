#eurosport specific functions

from lex_tools import try_ex,parse_date

def parse_data(intent_request):
    slots_dictionary = {}
    #campaigns
    campaigns = []
    for key in intent_request['currentIntent']['slots'].keys():
        if "Campaign" in key:
            value = validate_ES_campaign(try_ex(lambda: intent_request['currentIntent']['slots'][key]))
            if value != None:
                campaigns.append(value)


    if len(campaigns) == 0:
        campaigns = None

    slots_dictionary["campaigns"] = campaigns

    #media
    media = []
    for key in intent_request['currentIntent']['slots'].keys():
        if "Media" in key:
            value = try_ex(lambda: intent_request['currentIntent']['slots'][key])
            if value != None:
                media.append(value)

    if len(media) == 0:
        media = None

    slots_dictionary["media"] = media
    #markets

    markets = []

    for key in intent_request['currentIntent']['slots'].keys():
        if "Market" in key:
            value = validate_ES_country(try_ex(lambda: intent_request['currentIntent']['slots'][key]))
            if value != None:
                markets.append(value)


    if len(markets) == 0:
        markets = None

    slots_dictionary["markets"] = markets

    dimensions = []

    for key in intent_request['currentIntent']['slots'].keys():
        if "Dimension" in key:
            value = try_ex(lambda: intent_request['currentIntent']['slots'][key])
            if value != None:
                dimensions.append(value)


    if len(dimensions) == 0:
        dimensions = None

    slots_dictionary["dimensions"] = dimensions

    #date parse
    date = try_ex(lambda: intent_request['currentIntent']['slots']['Date'])
    if date != None:
        date = parse_date(str(date),past=True)

    slots_dictionary['date'] = date

    #metric
    slots_dictionary['Metric'] = try_ex(lambda: intent_request['currentIntent']['slots']['Metric'])

    #extreme
    slots_dictionary['Extreme'] = try_ex(lambda: intent_request['currentIntent']['slots']['Extreme'])

    return slots_dictionary

def validate_ES_campaign(campaign):
    if campaign is None:
        return None
    if "undesliga" in campaign:
        return "FootballBundesliga"
    if "asket" in campaign:
        return "Basket%"
    if "lympics" in campaign:
        return "WintersportsOlympics"
    if "quash" in campaign:
        return "SquashPsaworldtour"

def validate_ES_country(country):
    if country is None:
        return None
    if country in ['Germany','DE']:
        return 'DE'
    if country in ['NO','Norway']:
        return 'NO'
    if country in ['UK','GB','United Kingdom','Great Britain']:
        return 'UK'
    if country in ['IT','Italy']:
        return 'IT'
    if country in ['France','FR']:
        return 'FR'
    if country in ['NL','Netherlands']:
        return 'NL'
    if country in ['All','All Countries']:
        return 'SELECT DISTINCT(Countries) FROM prod_channels'
    if country in ['Nordics']:
        return "DK','NO','SE','FI"
    else:
        return country


def prod_channels_query(query,campaigns,markets,date,media,dimensions):

    #define the group by
    if dimensions != None:
        for dimension in dimensions:
            query += " , " + dimension


    query += " FROM prod_channels"
    #define the where
    if campaigns != None or markets != None or media != None or date != None:
        query += " WHERE"
        previous_where = False

        if media != None:
            if previous_where == True:
                query += " AND"
            media_seq = media[0]
            for channel in media[1:]:
                media_seq += "','" + channel
            query += " Channel in ('" + media_seq + "')"
            previous_where = True

        if campaigns != None:
            if previous_where == True:
                query += " AND"
            campaign_seq = campaigns[0]
            for campaign in campaigns[1:]:
                campaign_seq += "','" + campaign
            query += " Campaign in ('" + campaign_seq + "')"
            previous_where = True

        if markets != None:
            if previous_where == True:
                query += " AND"
            market_seq = markets[0]
            for market in markets[1:]:
                market_seq += "','" + market
            query += " Country in ('" + market_seq + "')"
            previous_where = True

        if date != None:
            if previous_where == True:
                query += " AND"
            query += " Date >= '" + date['start'] + "' and Date <= '" + date['end'] + "'"
            previous_where = True

    #define the group by
    if dimensions != None:
        query += " GROUP BY "
        previous_groupby = False
        for dimension in dimensions:
            if previous_groupby == False:
                query += dimension
            else:
                query += " , " + dimension

            previous_groupby = True

    query += ";"

    return query


def generate_metric_query(metric,intent_request):
    if metric == "Spend":
        #preprocess the request data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Spend) as Spend"
        query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query
    elif metric == "Subscriptions":
        #proproces the requrest data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Conversions) as Subscriptions"
        query = query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query
    elif metric == "SAC":
        #process the request data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Spend)/sum(Conversions) as SAC"
        query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query
    elif metric == "CTR":
        #proproces the requrest data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Clicks)/sum(Impressions)*100 as CTR"
        query =  prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query

    elif metric == "CVR":
        #proproces the requrest data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Conversions)/sum(Clicks)*100 as CVR"
        query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query

    elif metric == "CPC":
        #proproces the requrest data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Spend)/sum(Clicks)*100 as CPC"
        query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query

def comparison_query(intent_request):

    slots_dictionary = parse_data(intent_request)

    if slots_dictionary['Metric'] == "SAC":

        query = "SELECT SUM(Spend)/SUM(Conversions) as SAC"

    elif slots_dictionary['Metric'] == "Subscriptions":

        query = "SELECT SUM(Conversions) as Subscriptions"

    elif slots_dictionary['Metric'] == "CVR":

        query = "SELECT SUM(Conversions)/SUM(Impressions)*100 as CVR"

    elif slots_dictionary['Metric'] == "CTR":

        query = "SELECT SUM(Clicks)/SUM(Impressions)*100 as CTR"

    elif slots_dictionary['Metric'] == "CPC":

        query = "SELECT SUM(Spend)/SUM(Clicks) as CTR"

    dimensions = slots_dictionary['dimensions']

    if dimensions != None:
        for dimension in dimensions:
            query += " , " + dimension

    query += " FROM prod_channels"

    campaigns = slots_dictionary['campaigns']
    markets = slots_dictionary['markets']
    media = slots_dictionary['media']
    date = slots_dictionary['date']

    if campaigns != None or markets != None or media != None or date != None or slots_dictionary['Metric'] in ["SAC","Subscriptions"]:
        query += " WHERE"
        previous_where = False

        if media != None:
            if previous_where == True:
                query += " AND"
            media_seq = media[0]
            for channel in media[1:]:
                media_seq += "','" + channel
            query += " Channel in ('" + media_seq + "')"
            previous_where = True

        if campaigns != None:
            if previous_where == True:
                query += " AND"
            campaign_seq = campaigns[0]
            for campaign in campaigns[1:]:
                campaign_seq += "','" + campaign
            query += " Campaign in ('" + campaign_seq + "')"
            previous_where = True

        if markets != None:
            if previous_where == True:
                query += " AND"
            market_seq = markets[0]
            for market in markets[1:]:
                market_seq += "','" + market
            query += " Country in ('" + market_seq + "')"
            previous_where = True

        if date != None:
            if previous_where == True:
                query += " AND"
            query += " Date >= '" + date['start'] + "' and Date <= '" + date['end'] + "'"
            previous_where = True


        if slots_dictionary['Metric'] == "SAC":
            if previous_where == True:
                query += " AND"
            query += " Spend > 0 and Conversions > 0"
            previous_where = True

        if slots_dictionary['Metric'] == "Subscriptions":
            if previous_where == True:
                query += " AND"
            query += " Conversions > 0"
            previous_where = True


    if dimensions != None:
        query += " GROUP BY "
        previous_groupby = False
        for dimension in dimensions:
            if previous_groupby == False:
                query += dimension
            else:
                query += " , " + dimension

    if slots_dictionary['Extreme'] == "best":
        if slots_dictionary['Metric'] == "SAC":

            query += " ORDER BY SAC ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "Subscriptions":

            query += " ORDER BY Subscriptions DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CVR":

            query += " ORDER BY CVR DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CTR":

            query += " ORDER BY CTR DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CPC":

            query += " ORDER BY CPC ASC LIMIT 1;"

        else:
            query = None

    elif slots_dictionary['Extreme'] == "worst":
        if slots_dictionary['Metric'] == "SAC":

            query += " ORDER BY SAC DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "Subscriptions":

            query += " ORDER BY Subscriptions ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CVR":

            query += " ORDER BY CVR ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CTR":

            query += " ORDER BY CTR ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CPC":

            query += " ORDER BY CPC DESC LIMIT 1;"

        else:
            query = None

    elif slots_dictionary['Extreme'] == "most":
        if slots_dictionary['Metric'] == "SAC":

            query += " ORDER BY SAC DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "Subscriptions":

            query += " ORDER BY Subscriptions DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CVR":

            query += " ORDER BY CVR DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CTR":

            query += " ORDER BY CTR DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CPC":

            query += " ORDER BY CPC DESC LIMIT 1;"

        else:
            query = None

    elif slots_dictionary['Extreme'] == "least":
        if slots_dictionary['Metric'] == "SAC":

            query += " ORDER BY SAC ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "Subscriptions":

            query += " ORDER BY Subscriptions ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CVR":

            query += " ORDER BY CVR ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CTR":

            query += " ORDER BY CTR ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CPC":

            query += " ORDER BY CPC ASC LIMIT 1;"

        else:
            query = None


    print query
    return query
