import datetime
import dateutil.parser
import calendar

def parse_date(lex_date,past):

    if lex_date.count("-") == 2: #regular %Y-%m-%d or %Y-w%U-WE
        if lex_date.split("-")[2] != "WE":
            start = lex_date
            end = lex_date
        else:
            if "W" in lex_date.split("-")[1]: #%Y-w%W
                #parse the Sunday
                end = datetime.datetime.strptime(lex_date + '-0', "%Y-W%U-WE-%w")
                #parse the Saturday
                start = end - datetime.timedelta(days=1)
                start = str(start.date())
                end = str(end.date())

    elif lex_date.count("-") == 1:
        if "W" in lex_date.split("-")[1]: #%Y-w%W
            #parse the Sunday
            start = datetime.datetime.strptime(lex_date + '-0', "%Y-w%U-%w")
            start -= datetime.timedelta(weeks=1)
            start = str(start.date())
            #Parse the Saturday
            end = datetime.datetime.strptime(lex_date + '-6', "%Y-w%U-%w")
            end -= datetime.timedelta(weeks=1)
            end = str(end.date())


        else: #month EUROSPORT Specific!!
            year = int(lex_date.split("-")[0])
            month = int(lex_date.split("-")[1])
            if past == True:
                current_year = int(datetime.datetime.strftime(datetime.datetime.now(),"%Y"))
                current_month = int(datetime.datetime.strftime(datetime.datetime.now(),"%m"))

                if datetime.datetime.strptime(lex_date,"%Y-%m") > datetime.datetime.now():
                    if year > current_year:
                        year = current_year
            start = str(year) + "-" + str(month).zfill(2) + "-01"
            end = str(year) + "-" + str(month).zfill(2) + "-" + str(calendar.monthrange(year,month)[1]).zfill(2)

    elif lex_date.count("-") == 0 and len(lex_date) == 4 and isinstance(int(lex_date),int):
        start = lex_date + "-01-01"
        end = lex_date + "-12-31"

    try:

        if datetime.datetime.strptime(start,'%Y-%m-%d') > datetime.datetime.now():
            print "Trying a date in the future, not good..."
            raise ValueError("Sorry, I couldn't really understand the date range. Please rephrase your question.")
        else:
            try:
                return {"start":start,"end":end}
            except:
                raise ValueError("Sorry, I couldn't really understand the date range. Please rephrase your question.")
    except:
        raise ValueError("Sorry, I couldn't really understand the date range. Please rephrase your question.")

def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit, message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message
        }
    }


def confirm_intent(session_attributes, intent_name, slots, message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_name,
            'slots': slots,
            'message': message
        }
    }


def close(session_attributes, fulfillment_state, message):
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    }

    return response


def delegate(session_attributes, slots):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Delegate',
            'slots': slots
        }
    }

def safe_int(n):
    """
    Safely convert n value to int.
    """
    if n is not None:
        return int(n)
    return n


def try_ex(func):
    """
    Call passed in function in try block. If KeyError is encountered return None.
    This function is intended to be used to safely access dictionary.

    Note that this function would have negative impact on performance.
    """

    try:
        return func()
    except KeyError:
        return None

def get_day_difference(later_date, earlier_date):
    later_datetime = dateutil.parser.parse(later_date).date()
    earlier_datetime = dateutil.parser.parse(earlier_date).date()
    return abs(later_datetime - earlier_datetime).days


def add_days(date, number_of_days):
    new_date = dateutil.parser.parse(date).date()
    new_date += datetime.timedelta(days=number_of_days)
    return new_date.strftime('%Y-%m-%d')
