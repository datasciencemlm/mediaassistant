endite#!/bin/bash
#A script to package the mediaassistant function as a lambda
# function and push it to the cloud

echo "Copying function"
cp media_assistant/media_assistant.py lambda/media_assistant.py
cp media_assistant/eurosport.py lambda/eurosport.py
cp media_assistant/lex_tools.py lambda/lex_tools.py
cp media_assistant/validation.py lambda/validation.py

echo "compressing file"
cd lambda
zip -r lambda.zip *

#pushing to aws
aws lambda update-function-code \
--function-name=mlmdata-MediaAssistant \
--zip-file=fileb://lambda.zip
cd ..
echo "Done!"
