#testing conducted with pytest
import pytest
from media_assistant import *
import eurosport
from lex_tools import parse_date
import pymysql.cursors

def test_querygenerator():
    intent_req = {'currentIntent':{'slots':{}}}
    assert eurosport.generate_metric_query(metric="Spend",intent_request=intent_req) == "SELECT sum(Spend) as Spend FROM prod_channels;"

def test_querygenerator_2():
    intent_req = {'currentIntent':{'slots':{'Market':'DE'}}}
    assert eurosport.generate_metric_query(metric="Spend",intent_request=intent_req) == "SELECT sum(Spend) as Spend FROM prod_channels WHERE Country in ('DE');"


def test_queryresult():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Metric":"Subscriptions","Market": "Nordics","MarketTwo": "DE","MarketThree":"NL","Campaign": "WintersportsOlympics","CampaignTwo":None,"Date":None}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs FROM prod_channels WHERE COUNTRY IN ('DE','DK','NO','SE','FI','NL') AND Campaign in ('WintersportsOlympics');"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(int(query_result['Subs'])) + ' subscriptions.' + ' this was for the Subscriptions for all markets for all campaigns for all time.'
            db.close()

    result = lambda_handler(intent_req,None)
    assert result['dialogAction']['message']['content'] == query_result

def test_null_query():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Metric":"Subscriptions","Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Date":None}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs FROM prod_channels;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(int(query_result['Subs'])) + ' subscriptions.' + ' this was for the Subscriptions for all markets for all campaigns for all time.'
            db.close()

    result = lambda_handler(intent_req,None)
    assert result['dialogAction']['message']['content'] == query_result

def test_query_with_breakdown_dimensions():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": "DE","MarketTwo": "IT","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":None,"Metric":"Subscriptions"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    result = lambda_handler(intent_req,None)

    assert isinstance(result['dialogAction']['message']['content'],str)

def test_date_week():
    assert parse_date("2018-W01",past=True) == {"start":"2017-12-31","end":"2018-01-06"}

def test_date_single_day():
    assert parse_date("2018-01-01",past=True) == {"start":"2018-01-01","end":"2018-01-01"}

def test_month():
    assert parse_date("2018-01",past=True) == {"start":"2018-01-01","end":"2018-01-31"} and parse_date("2018-02",past=True) == {"start":"2018-02-01","end":"2018-02-28"}

def test_year():
    assert parse_date("2018",past=True) == {"start":"2018-01-01","end":"2018-12-31"} and parse_date("2017",past=True) == {"start":"2017-01-01","end":"2017-12-31"}

def test_year_result():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": "DE","MarketTwo": "IT","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2017","Metric":"Subscriptions"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs FROM prod_channels WHERE COUNTRY IN ('DE','IT') AND Date >= '2017-01-01' and Date <= '2017-12-31'"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(int(query_result['Subs'])) + ' subscriptions.' + ' this was for the Subscriptions for all markets for all campaigns from 2017-01-01 to 2017-12-31.'
            db.close()

    result = lambda_handler(intent_req,None)
    assert result['dialogAction']['message']['content'] == query_result

def test_bare_month_name():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2019-01","Metric":"Subscriptions"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs FROM prod_channels WHERE Date >= '2018-01-01' and Date <= '2018-01-31';"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(int(query_result['Subs'])) + ' subscriptions.' + " this was for the Subscriptions for all markets for all campaigns from 2018-01-01 to 2018-01-31."
            db.close()

    result = lambda_handler(intent_req,None)
    assert result['dialogAction']['message']['content'] == query_result

def test_bare_month_name_spend():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2017-10","Metric":"Spend"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend) as Spend FROM prod_channels WHERE Date >= '2017-10-01' and Date <= '2017-10-31';"
            cursor.execute(query)
            query_result = cursor.fetchone()
            print query_result
            query_result = str(round(query_result['Spend'],2)) + ' Euro.'
            db.close()

    result = lambda_handler(intent_req,None)
    assert "this was" in result['dialogAction']['message']['content']


def test_sac_dimensions():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Country","Date":None,"Metric":"SAC"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)

    result = lambda_handler(intent_req,None)
    assert isinstance(result['dialogAction']['message']['content'],str)

def test_weekend_spend():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"MarketTwo": None,"MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2018-W06-WE","Metric":"Spend"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend) as Spend FROM prod_channels WHERE Date >= '2018-02-10' and Date <= '2018-02-11';"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = str(round(query_result['Spend'],2)) + ' Euro.' + " this was for the Spend for all markets for all campaigns from 2018-02-10 to 2018-02-11."
            db.close()

    result = lambda_handler(intent_req,None)
    assert result['dialogAction']['message']['content'] == query_result

def test_find_best_Subs():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": None,"Metric":"Subscriptions","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":"2018-W06-WE","Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Conversions) as Subs , Campaign FROM prod_channels WHERE Date >= '2018-02-10' and Date <= '2018-02-11' GROUP BY Campaign ORDER BY Subs DESC LIMIT 1;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "Campaign: WintersportsOlympics, Subscriptions: " + str(int(query_result['Subs'])) + "." + ' this was for the campaign with the best subscriptions for all markets for all campaigns from 2018-02-10 to 2018-02-11.'
            db.close()

    result = lambda_handler(intent_req,None)
    assert result['dialogAction']['message']['content'] == query_result


def test_find_best_SAC_best():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": None,"Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":None,"Extreme":"best"}}}
    result = lambda_handler(intent_req,None)
    assert "this was for" in result['dialogAction']['message']['content']

def test_find_best_SAC_worst():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": None,"Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":None,"Extreme":"worst"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC , Campaign FROM prod_channels WHERE Spend > 0 and Conversions > 0 GROUP BY Campaign ORDER BY SAC DESC LIMIT 1;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEurolegue"  + "."
            db.close()

    result = lambda_handler(intent_req,None)
    assert "this was for" in result['dialogAction']['message']['content']

def test_find_best_SAC_worst_week():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": None,"Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Campaign","Date":"2018-W6","Extreme":"worst"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC , Campaign FROM prod_channels WHERE Spend > 0 and Conversions > 0 and Date >= '2018-02-04' and Date <= '2018-02-10' GROUP BY Campaign ORDER BY SAC DESC LIMIT 1;"
            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = lambda_handler(intent_req,None)
    assert "this was for" in result['dialogAction']['message']['content']

def test_SAC_DE_last_week():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": "DE","Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2018-W6","Extreme":"worst"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Country = 'DE' and Date >= '2018-02-04' and Date <= '2018-02-10';"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = lambda_handler(intent_req,None)
    assert "this was for" in result['dialogAction']['message']['content']


def test_SAC_DE_last_week():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": "DE","Metric":"SAC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Channel","Date":"2018-W07-WE","Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Country = 'DE' and Date >= '2018-02-04' and Date <= '2018-02-10';"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = lambda_handler(intent_req,None)

    assert result['dialogAction']['message']['content'] != "Oops, I had trouble understanding some of the question. Could you please ask it again a little differently? For example, instead of saying 'over the weekend', you could say 'last weekend'."

def test_CTR_DE_last_week_best():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": "DE","Metric":"CTR","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Channel","Date":"2018-W06-WE","Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Country = 'DE' and Date >= '2018-02-04' and Date <= '2018-02-10';"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "." + " this was for the channel with the best ctr in DE for all campaigns from 2018-02-10 to 2018-02-11."
            db.close()

    result = lambda_handler(intent_req,None)
    assert "this was for" in result['dialogAction']['message']['content']

def test_CVR_DE_last_week_best():
    intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": "DE","Metric":"CVR","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Channel","Date":"2018-W06-WE","Extreme":"best"}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Country = 'DE' and Date >= '2018-02-04' and Date <= '2018-02-10';"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "SAC: " + str(round(query_result['SAC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = lambda_handler(intent_req,None)
    assert "this was for" in result['dialogAction']['message']['content']


def test_greeting():
    intent_req = {"currentIntent": {"name": "Greeting","slots": {"statusCheck":"How are you"}}}
    result = lambda_handler(intent_req,None)
    assert result['dialogAction']['message']['content'] == 'Hey, good thanks. What would you like to know?'

def test_greeting():
    intent_req = {"currentIntent": {"name": "Greeting","slots": {"statusCheck":"How are you"}}}
    result = lambda_handler(intent_req,None)
    assert "?" in result['dialogAction']['message']['content']


def test_greeting_no_status_check():
    intent_req = {"currentIntent": {"name": "Greeting","slots": {"statusCheck":None}}}
    result = lambda_handler(intent_req,None)
    assert "?" in result['dialogAction']['message']['content']


def test_CPC():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"Metric":"CPC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":None,"Extreme":None}}}

    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    with db.cursor() as cursor:
            query = "SELECT SUM(Spend)/SUM(Clicks) as CPC FROM prod_channels;"

            cursor.execute(query)
            query_result = cursor.fetchone()
            query_result = "CTR: " + str(round(query_result['CPC'],2)) + ", Campaign: BasketballEuroleague"  + "."
            db.close()

    result = lambda_handler(intent_req,None)
    assert "Euro per click" in result['dialogAction']['message']['content']

def test_lex():
    event = {
  "session": {
    "new": true,
    "sessionId": "SessionId.c5e4a510-50fb-45bb-adbb-2bf59c8342a4",
    "application": {
      "applicationId": "amzn1.ask.skill.0594091d-d63c-4122-8106-47d4e63db5b1"
    },
    "attributes": {},
    "user": {
      "userId": "amzn1.ask.account.AG74SOBKMBJJZT2KKCWXTISFDR5MBMLHO65525FFK3PEY53Y7Z5YYZ6RQNFP2DM43KA2NKNEIARKTUK3ITJAY75CGVVF3WQUBZ55POEWQG7FS5I4UIUQFLF3URUWFZJ6DKVQ4H6OEGCTALO6KVWOMIUVMXM6IWMFJTJ7GUBXMIE54O5OX4RK7SAMNQT5IPENU3JXYG7PCJGE7BY"
    }
  },
  "request": {
    "type": "IntentRequest",
    "requestId": "EdwRequestId.097ddffc-9073-484e-9240-b482ba43175a",
    "intent": {
      "name": "SingleMetric",
      "slots": {
        "CampaignTwo": {
          "name": "CampaignTwo"
        },
        "MediaTwo": {
          "name": "MediaTwo"
        },
        "Campaign": {
          "name": "Campaign"
        },
        "Media": {
          "name": "Media"
        },
        "MarketTwo": {
          "name": "MarketTwo"
        },
        "DimensionTwo": {
          "name": "DimensionTwo"
        },
        "Metric": {
          "name": "Metric",
          "value": "spend"
        },
        "Dimension": {
          "name": "Dimension"
        },
        "Market": {
          "name": "Market",
          "value": "Germany"
        },
        "Date": {
          "name": "Date",
          "value": "2018-02-14"
        },
        "MarketThree": {
          "name": "MarketThree"
        }
      }
    },
    "locale": "en-GB",
    "timestamp": "2018-02-15T14:21:53Z"
  },
  "context": {
    "AudioPlayer": {
      "playerActivity": "IDLE"
    },
    "System": {
      "application": {
        "applicationId": "amzn1.ask.skill.0594091d-d63c-4122-8106-47d4e63db5b1"
      },
      "user": {
        "userId": "amzn1.ask.account.AG74SOBKMBJJZT2KKCWXTISFDR5MBMLHO65525FFK3PEY53Y7Z5YYZ6RQNFP2DM43KA2NKNEIARKTUK3ITJAY75CGVVF3WQUBZ55POEWQG7FS5I4UIUQFLF3URUWFZJ6DKVQ4H6OEGCTALO6KVWOMIUVMXM6IWMFJTJ7GUBXMIE54O5OX4RK7SAMNQT5IPENU3JXYG7PCJGE7BY"
      },
      "device": {
        "supportedInterfaces": {}
      }
    }
  },
  "version": "1.0"
}

    result = lambda_handler(event,None)
    assert isinstance(result,dict)

def test_date_comparison_dispatch():
    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    assert "respectively" in (dispatch({"currentIntent": {"name": "ComparativePerformance","slots": {"Metric":"SAC","DateComparison":"week-on-week"}}},db))

def test_date_comparison_dispatch():
    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    assert "respectively" in (dispatch({"currentIntent": {"name": "ComparativePerformance","slots": {"Metric":"Spend","DateComparison":"week-on-week","Campaign":"WintersportsOlympics"}}},db))
    #raise Exception('Intent with name ' + intent_name + ' not supported')

def test_yesterday_spend():
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": None,"Metric":"Spend","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":None,"Date":"2018-02-19"}}}


    result = lambda_handler(intent_req,None)
    assert "this was" in result['dialogAction']['message']['content']
