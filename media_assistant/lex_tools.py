import datetime
import dateutil.parser
from monthdelta import monthdelta
import calendar
import pymysql.cursors


def connect_to_db(client):
    if client == 9999:
        return pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                     user='redash',
                                     password='Explode-Cape-Reproduction-Ground-Talk-5',
                                     db='client_9999',
                                     charset='utf8mb4',
                                     cursorclass=pymysql.cursors.DictCursor)

def query_db(db,query):
    try:
        with db.cursor() as cursor:
            cursor.execute(query)
            result = cursor.fetchall()
            db.close()
    except:
        result = "Sorry, I'm having trouble reaching the datasource"
    return result

def conversational_date(date_dict):
    if date_dict["start"] == date_dict["end"]:
        return str(date_dict["start"])
    else:
        return str(date_dict["start"]) + " to " + str(date_dict["end"])

def test_conversational_date_yesterday():
    assert conversational_date({"start":"2018-02-19","end":"2018-02-19"}) == "2018-02-19"

def parse_date_comparison(lex_date):

    now = datetime.datetime.now()
    if lex_date == "week-on-week":
        a_start = str(datetime.datetime.strptime(now.strftime("%Y-%U") + "-0","%Y-%U-%w").date())
        a_end = str(datetime.datetime.strptime(now.strftime("%Y-%U") + "-6","%Y-%U-%w").date())

        last_week = now - datetime.timedelta(weeks=1)
        b_start = str(datetime.datetime.strptime(last_week.strftime("%Y-%U") + "-0","%Y-%U-%w").date())
        b_end = str(datetime.datetime.strptime(last_week.strftime("%Y-%U") + "-6","%Y-%U-%w").date())

        return {"group_a":{"start":a_start,"end":a_end},"group_b":{"start":b_start,"end":b_end}}

    elif lex_date == "month-on-month":
        current_year = int(now.strftime("%Y"))
        current_month = int(now.strftime("%m"))
        current_month_end = str(str(calendar.monthrange(current_year,current_month)[1]).zfill(2))

        a_start = str(datetime.datetime.strptime(now.strftime("%Y-%m") + "-01","%Y-%m-%d").date())
        a_end = str(datetime.datetime.strptime(now.strftime("%Y-%m") + "-" + current_month_end ,"%Y-%m-%d").date())

        last_month = now - monthdelta(1)
        year = int(last_month.strftime("%Y"))
        month = int(last_month.strftime("%m"))
        month_end = str(str(calendar.monthrange(year,month)[1]).zfill(2))

        b_start = str(datetime.datetime.strptime(last_month.strftime("%Y-%m") + "-01","%Y-%m-%d").date())
        b_end = str(datetime.datetime.strptime(last_month.strftime("%Y-%m") + "-" + month_end ,"%Y-%m-%d").date())

        return {"group_a":{"start":a_start,"end":a_end},"group_b":{"start":b_start,"end":b_end}}

def test_parse_date_comparison_week():
    parsed_date = parse_date_comparison("week-on-week")
    desired_result = {"group_a":{"start":"2018-02-18","end":"2018-02-24"},"group_b":{"start":"2018-02-11","end":"2018-02-17"}}

    assert parse_date_comparison("week-on-week") == desired_result

def test_parse_date_comparison_month():
    parsed_date = parse_date_comparison("month-on-month")
    desired_result = {"group_a":{"start":"2018-02-01","end":"2018-02-28"},"group_b":{"start":"2018-01-01","end":"2018-01-31"}}

    assert parse_date_comparison("month-on-month") == desired_result

def parse_date(lex_date,past):

    if lex_date.count("-") == 2: #regular %Y-%m-%d or %Y-w%U-WE
        if lex_date.split("-")[2] != "WE":
            start = lex_date
            end = lex_date
        else:
            if "W" in lex_date.split("-")[1]: #%Y-w%W
                #parse the Sunday
                end = datetime.datetime.strptime(lex_date + '-0', "%Y-W%U-WE-%w")
                #parse the Saturday
                start = end - datetime.timedelta(days=1)
                start = str(start.date())
                end = str(end.date())

    elif lex_date.count("-") == 1:
        if "W" in lex_date.split("-")[1]: #%Y-w%W
            #parse the Sunday
            start = datetime.datetime.strptime(lex_date + '-0', "%Y-w%U-%w")
            start -= datetime.timedelta(weeks=1)
            start = str(start.date())
            #Parse the Saturday
            end = datetime.datetime.strptime(lex_date + '-6', "%Y-w%U-%w")
            end -= datetime.timedelta(weeks=1)
            end = str(end.date())


        else: #month EUROSPORT Specific!!
            year = int(lex_date.split("-")[0])
            month = int(lex_date.split("-")[1])
            if past == True:
                current_year = int(datetime.datetime.strftime(datetime.datetime.now(),"%Y"))
                current_month = int(datetime.datetime.strftime(datetime.datetime.now(),"%m"))

                if datetime.datetime.strptime(lex_date,"%Y-%m") > datetime.datetime.now():
                    if year > current_year:
                        year = current_year
            start = str(year) + "-" + str(month).zfill(2) + "-01"
            end = str(year) + "-" + str(month).zfill(2) + "-" + str(calendar.monthrange(year,month)[1]).zfill(2)

    elif lex_date.count("-") == 0 and len(lex_date) == 4 and isinstance(int(lex_date),int):
        start = lex_date + "-01-01"
        end = lex_date + "-12-31"

    try:

        if datetime.datetime.strptime(start,'%Y-%m-%d') > datetime.datetime.now():
            print "Trying a date in the future, not good..."
            raise ValueError("Sorry, I couldn't really understand the date range. Please rephrase your question.")
        else:
            try:
                return {"start":start,"end":end}
            except:
                raise ValueError("Sorry, I couldn't really understand the date range. Please rephrase your question.")
    except:
        raise ValueError("Sorry, I couldn't really understand the date range. Please rephrase your question.")

def elicit_slot(session_attributes, intent_name, slots, slot_to_elicit, message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ElicitSlot',
            'intentName': intent_name,
            'slots': slots,
            'slotToElicit': slot_to_elicit,
            'message': message
        }
    }


def confirm_intent(session_attributes, intent_name, slots, message):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'ConfirmIntent',
            'intentName': intent_name,
            'slots': slots,
            'message': message
        }
    }


def close(session_attributes, fulfillment_state, message):
    response = {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Close',
            'fulfillmentState': fulfillment_state,
            'message': message
        }
    }

    return response


def delegate(session_attributes, slots):
    return {
        'sessionAttributes': session_attributes,
        'dialogAction': {
            'type': 'Delegate',
            'slots': slots
        }
    }

def safe_int(n):
    """
    Safely convert n value to int.
    """
    if n is not None:
        return int(n)
    return n


def try_ex(func):
    """
    Call passed in function in try block. If KeyError is encountered return None.
    This function is intended to be used to safely access dictionary.

    Note that this function would have negative impact on performance.
    """

    try:
        return func()
    except KeyError:
        return None

def get_day_difference(later_date, earlier_date):
    later_datetime = dateutil.parser.parse(later_date).date()
    earlier_datetime = dateutil.parser.parse(earlier_date).date()
    return abs(later_datetime - earlier_datetime).days


def add_days(date, number_of_days):
    new_date = dateutil.parser.parse(date).date()
    new_date += datetime.timedelta(days=number_of_days)
    return new_date.strftime('%Y-%m-%d')
