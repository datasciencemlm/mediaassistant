"""
This sample demonstrates an implementation of the Lex Code Hook Interface
in order to serve a sample bot which manages reservations for hotel rooms and car rentals.
Bot, Intent, and Slot models which are compatible with this sample can be found in the Lex Console
as part of the 'BookTrip' template.

For instructions on how to set up and test this bot, as well as additional samples,
visit the Lex Getting Started documentation http://docs.aws.amazon.com/lex/latest/dg/getting-started.html.
"""

import json

import logging
import random
from lex_skill import lex_skill
from lex_tools import connect_to_db

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)

#js compatibility
true = True
null = "NULL"

# --- Client package

from eurosport import *

""" --- Functions that control the bot's behavior --- """



def return_text(text):
    return {"dialogAction": {"type": "Close","fulfillmentState": "Fulfilled","message": {"contentType": "PlainText","content": text}}}

def return_lex_response(text,event):
    version = event["version"]
    result = json.loads(json.dumps({"version": "1.0","sessionAttributes": {"sessionId": event['session']['sessionId']},"response": {"outputSpeech": {"type": "PlainText","text": text}, "shouldEndSession": False}}))
    return result

#a function for compatibility between lex and alexa
def alexify(event):
    intent_request = event['request']
    intent_request['currentIntent'] = intent_request['intent']

    for counter,intent in enumerate(lex_skill['languageModel']['intents']):
        if intent['name'] == intent_request['currentIntent']['name']:
            slots = lex_skill['languageModel']['intents'][counter]["slots"]


    #restructure the slot key value pairing
    for slot in intent_request['currentIntent']['slots'].keys():
        if "value" in intent_request['currentIntent']['slots'][slot].keys():
            intent_request['currentIntent']['slots'][slot] = intent_request['currentIntent']['slots'][slot]["value"]
        else:
            intent_request['currentIntent']['slots'][slot] = None

    #make the synomyms rectify to the original value
    new_value = None
    for request_slot in intent_request['currentIntent']['slots'].keys():
        if intent_request['currentIntent']['slots'][request_slot] != None:

            for slot in slots:
                if request_slot == slot['name']:
                    if "AMAZON" in slot['type']:
                        new_value = "synthetically filled"
                    else:
                        slot_type = slot['type']

                        for counter,slot_type_record in enumerate(lex_skill['languageModel']['types']):
                            if slot_type == lex_skill['languageModel']['types'][counter]['name']:
                                found_slot_type_record = lex_skill['languageModel']['types'][counter]
                                for value_counter,value_option in enumerate(found_slot_type_record['values']):

                                    valid_options = [found_slot_type_record['values'][value_counter]['name']['value']] + found_slot_type_record['values'][value_counter]['name']['synonyms']

                                    if any(s.lower() == intent_request['currentIntent']['slots'][request_slot].lower() for s in valid_options):
                                        new_value = found_slot_type_record['values'][value_counter]['name']['value']
                                        intent_request['currentIntent']['slots'][request_slot] = new_value

            if new_value == None:
                print intent_request['currentIntent']['slots'][request_slot]
                raise ValueError("Invalid slot value")

        new_value = None


    resultant_dict = {}
    resultant_dict['currentIntent'] = intent_request['currentIntent']

    return resultant_dict

# --- Main handler ---

def lambda_handler(event, context):
    """
    Route the incoming request based on intent.
    The JSON body of the request is provided in the event slot.
    """
    #fixes a js bug from alexa

    if 'currentIntent' in event.keys():

        #logger.debug('event.bot.name={}'.format(event['bot']['name']))
        try:
            return return_text(dispatch(event,connect_to_db(9999)))
        except:
            return return_text("Oops, I had trouble understanding some of the question. Could you please ask it again a little differently? For example, instead of saying 'over the weekend', you could say 'last weekend'.")


    elif 'request' in event.keys():
        #try to change the format to be able to be parsed by the lex bot
        try:
            lexified_event = alexify(event)

        except:
            return return_lex_response("I couldn't understand that, could you try that question again.",event)

        try:
            response = dispatch(lexified_event)
            print response
            return return_lex_response(response,event)
        except:
            return return_lex_response("I couldn't understand that, could you try that question again.",event)
