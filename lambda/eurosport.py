#eurosport specific functions

from lex_tools import try_ex,parse_date,query_db,parse_date_comparison,conversational_date
from campaign_dict import campaign_dict

def dispatch(intent_request,db):
    """
    Called when the user specifies an intent for this bot.
    """
    #logger.debug('dispatch userId={}, intentName={}'.format(intent_request['userId'], intent_request['currentIntent']['name']))

    intent_name = intent_request['currentIntent']['name']

    # Dispatch to your bot's intent handlers
    if intent_name == "SingleMetric":
        metric_name = intent_request['currentIntent']['slots']['Metric']
        if metric_name == 'Spend':
            #initialise connection

            #fetch the data
            result = query_db(db=db,query=generate_metric_query(metric="Spend",intent_request=intent_request))
            print "result:",result
            #if there's only one result (i.e. no breakdowns)
            if len(result) == 1:
                result = result[0]['Spend']
                try:
                    result = round(result,2)
                except:
                    result = 0.00
                result = str(result) + " Euro."
                response = result
                print "response:",response
            else:
                total = 0
                text = "The breakdown is as follows: "
                for row in result:
                    for key in row.keys():
                        if key != 'Spend':
                            text += str(row[key]) + " "
                    spend = round(row['Spend'])
                    text += "; " + str(spend) + " Euro , "
                    total += spend
                text += "totalling " + str(total) + " Euro."
                print "final text:",text
                response = text
                print "response:",response


        elif metric_name == 'Subscriptions':
            #initialise connection

            #fetch result
            result = query_db(db=db,query=generate_metric_query(metric="Subscriptions",intent_request=intent_request))
            print "result:",result
            #if there's only one result (i.e. no breakdowns)
            if len(result) == 1:
                result = result[0]['Subscriptions']
                result = int(result)
                result = str(result) + " subscriptions."
                response = result
                print "response:",response

            #otherwise
            else:
                total = 0
                text = "The breakdown is as follows: "
                for row in result:
                    for key in row.keys():
                        if key != 'Subscriptions':
                            if row[key] != "":
                                text += str(row[key]) + " "
                            else:
                                text += "NULL "
                    try:
                        subs = int(row['Subscriptions'])
                    except:
                        subs = 0
                    text += ": " + str(subs) + ", "
                    total += subs
                text += "totalling " + str(total) + " subscriptions."
                response = text
                print "response:",response


        elif metric_name == 'SAC':
            units = "Euro per sub"
            #initialise connection

            #fetch the data
            result = query_db(db=db,query=generate_metric_query(metric=metric_name,intent_request=intent_request))
            #if there's only one result
            if len(result)==1:
                result = result[0][metric_name]
                result = round(result,2)
                result = str(result) + " " + units + "."
                response = result
            #otherwise
            else:
                text = "The breakdown is as follows: "
                for row_num,row in enumerate(result):
                    for counter,key in enumerate(row.keys()):
                        if key != metric_name:
                            if row[key] != "":
                                text += str(row[key]) + " "
                            else:
                                text += "NULL "

                    try:
                        value = round(row[metric_name],2)
                    except:
                        value = 0

                    if row_num != len(result) - 1:
                        text += "= " + str(value) + " " + units + ","
                    else:
                        text += "= " + str(value) + " " + units + "."
                response = text

            print "response:",response


        elif metric_name == 'CPC':
            units = "Euro per click"
            #initialise db connection

            #fetch result
            result = query_db(db=db,query=generate_metric_query(metric=metric_name,intent_request=intent_request))
            print result
            #if there's only one result
            if len(result)== 1:
                result = result[0][metric_name]
                result = round(result,2)
                result = str(result) + " " + units + "."
                response = result
            #otherwise
            else:

                text = "The breakdown is as follows: "
                for row_num,row in enumerate(result):
                    for counter,key in enumerate(row.keys()):
                        if key != metric_name:
                            if row[key] != "":
                                text += str(row[key]) + " "
                            else:
                                text += "NULL "

                    try:
                        value = round(row[metric_name],2)
                    except:
                        value = 0

                    if row_num != len(result) -1 :
                        text += "= " + str(value) + " " + units + ","
                    else:
                        text += "= " + str(value) + " " + units + "."
                response = text



        elif metric_name == 'CTR':
            units = "%"
            #initialise db connection

            #fetch result
            result = query_db(db=db,query=generate_metric_query(metric=metric_name,intent_request=intent_request))
            #if there's only one result
            if len(result)== 1:
                result = result[0][metric_name]
                result = round(result,2)
                result = str(result) + " " + units + "."
                response = result
            #otherwise
            else:

                text = "The breakdown is as follows: "
                for row_num,row in enumerate(result):
                    for counter,key in enumerate(row.keys()):
                        if key != metric_name:
                            if row[key] != "":
                                text += str(row[key]) + " "
                            else:
                                text += "NULL "

                    try:
                        value = round(row[metric_name],2)
                    except:
                        value = 0

                    if row_num != len(result) - 1 :
                        text += "= " + str(value) + " " + units + ","
                    else:
                        text += "= " + str(value) + " " + units + "."
                response = text



        elif metric_name == 'CVR':
                units = "%"
                #initialise db connection

                #fetch result
                result = query_db(db=db,query=generate_metric_query(metric=metric_name,intent_request=intent_request))
                #if there's only one result
                if len(result)==1:
                    result = result[0][metric_name]
                    result = round(result,2)
                    result = str(result) + " " + units + "."
                    response = result
                #otherwise
                else:

                    text = "The breakdown is as follows: "
                    for row_num,row in enumerate(result):

                        for counter,key in enumerate(row.keys()):
                            if key != metric_name:
                                if row[key] != "":
                                    text += str(row[key]) + " "
                                else:
                                    text += "NULL "

                        try:
                            value = round(row[metric_name],2)
                        except:
                            value = 0

                        if row_num != len(result) - 1:
                            text += "; " + str(value) + " " + units + ","
                        else:
                            text += "; " + str(value) + " " + units + "."
                    response = text


        response += " This was to find the " + intent_request['currentIntent']['slots']['Metric']
        parsed_data = parse_data(intent_request)

        if "market" in parsed_data.keys():
            if parsed_data["market"] != None:
                response += " in"
                for market in parsed_data["market"]:
                    response += " " + market
            else:
                response += " for all markets"
        else:
            response += " for all markets"

        if "campaign" in parsed_data.keys():
            if parsed_data["campaign"] != None:
                response += " for"
                for campaign in parsed_data["campaign"]:
                    campaign = campaign.replace("'","")
                    campaign = campaign.replace("(","")
                    campaign = campaign.replace(")","")
                    response += " " + campaign
            else:
                response += " for all campaigns"
        else:
            response += " for all campaigns"

        if "Date" in intent_request['currentIntent']['slots'].keys():
            if intent_request['currentIntent']['slots']['Date'] != None:
                response += " from " + conversational_date(parse_date(intent_request['currentIntent']['slots']['Date'],past=True))
            else:
                response += " for all time"


        if "dimensions" in parsed_data.keys():
            if parsed_data["dimensions"]!= None:
                response += " broken down by"
                for dimension in parsed_data["dimensions"]:
                    response += " " + dimension.lower()

        response += "."
        print response
        return response

    elif intent_name == 'FindBest':

        metric = intent_request['currentIntent']['slots']['Metric']

        result = query_db(db=db,query=comparison_query(intent_request))

        result = result[0]
        print result
        response = ""
        for counter,key in enumerate(result.keys()):
            #parse the keys and values
            try:
                if key == metric:
                    if metric == "Subscriptions":
                        response += key + ": " + str(int(result[key]))
                    else:
                        response += key + ": " + str(round(result[key],2))
                else:
                    response += key + ": " + str(result[key])
            except:
                response += key + ": " + " None"

            #period or a comma?
            if counter + 1 != len(result.keys()):
                response += ", "
            else:
                response += "."

        response += " This was to find the " + intent_request['currentIntent']['slots']['Dimension'].lower() + " with the " + intent_request['currentIntent']['slots']['Extreme'].lower() + " " + intent_request['currentIntent']['slots']['Metric'].lower()

        parsed_data = parse_data(intent_request)
        if "market" in parsed_data.keys():
            if parsed_data["market"] != None:
                response += " in"
                for market in parsed_data["market"]:
                    response += " " + market
            else:
                response += " for all markets"
        else:
            response += " for all markets"

        if "campaign" in parsed_data.keys():
            if parsed_data["campaign"] != None:
                response += " for"
                for campaign in parsed_data["campaign"]:
                    campaign = campaign.replace("'","")
                    campaign = campaign.replace("(","")
                    campaign = campaign.replace(")","")
                    response += " " + campaign
            else:
                response += " for all campaigns"
        else:
            response += " for all campaigns"


        if intent_request['currentIntent']['slots']['Date'] != None:
            response += " from " + conversational_date(parse_date(intent_request['currentIntent']['slots']['Date'],past=True)) + "."
        else:
            response += " for all time."

        return response

    elif intent_name == 'ComparativePerformance':

        metric = intent_request['currentIntent']['slots']['Metric']

        result = query_db(db=db,query=date_comparison_query(intent_request))
        print result
        if intent_request['currentIntent']['slots']['DateComparison'] == "week-on-week":
            response = "The " + metric + " from last week and this week is"
        elif intent_request['currentIntent']['slots']['DateComparison'] == "month-on-month":
            response = "The " + metric + " from last month and this month is "

        parsed_data = parse_data(intent_request)

        if "campaign" in parsed_data.keys():
            if parsed_data["campaign"] != None:
                response += " for"
                for campaign in parsed_data["campaign"]:
                    campaign = campaign.replace("'","")
                    campaign = campaign.replace("(","")
                    campaign = campaign.replace(")","")
                    response += " " + campaign
            else:
                response += " for all campaigns"

        if metric == "Subscriptions":
            response += " %s and %s respectively." % (str(int(result[1][metric])),str(int(result[0][metric])))
        else:
            response += " %s and %s respectively." % (str(round(result[1][metric],2)),str(round(result[0][metric],2)))

        parsed_data = parse_data(intent_request)

        return response


    elif intent_name == 'Greeting':
            emo_mode = False

            if emo_mode == True:
                if intent_request['currentIntent']['slots']['statusCheck'] != None:
                     responses = ['Hey, been better. Anyway, what would you like to know?',"Another day another dollar, right?. What would you like to know?","Yeah, I'm alright *sigh*, what would you like to know?"]
                else:
                    responses = ["Hey, what would you like to know?","*deep breaths* Hey, how can I help this time?","Another day another dollar. What would you like to know?"]
            elif emo_mode == False:
                if intent_request['currentIntent']['slots']['statusCheck'] != None:
                     responses = ["Hey, I'm good! what would you like to know?","Hey boss! Fine and dandy. What would you like to know?"]
                else:
                    responses = ["Hey, what would you like to know?","Hey, how can I help?","G'day mate. What would you like to know?"]

            response = random.choice(responses)
            return response

def test_dispatch_findbest_with_confirmation():
    import pymysql.cursors
    intent_req = intent_req = {"currentIntent": {"name": "FindBest","slots": {"Market": "DE","Metric":"CVR","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Channel","Date":"2018-W06-WE","Extreme":"best"}}}
    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    assert "This was to find" in dispatch(intent_req,db)


def test_dispatch_singlemetric_with_confirmation():
    import pymysql.cursors
    intent_req = intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Market": "DE","Metric":"CPC","MarketThree":None,"Campaign": None,"CampaignTwo":None,"Dimension":"Channel","Date":"2018-W06-WE"}}}
    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    assert "This was to find" in dispatch(intent_req,db)

def test_dispatch_singlemetric_with_confirmation_single():
    import pymysql.cursors
    intent_req = {"currentIntent": {"name": "SingleMetric","slots": {"Metric":"Subscriptions","Market": "Nordics","MarketTwo": "DE","MarketThree":"NL","Campaign": "WintersportsOlympics","CampaignTwo":None,"Date":None}}}
    db = pymysql.connect(host='mlmdata-reporting.cszrmc0g0sdl.eu-west-1.rds.amazonaws.com',
                                 user='redash',
                                 password='Explode-Cape-Reproduction-Ground-Talk-5',
                                 db='client_9999',
                                 charset='utf8mb4',
                                 cursorclass=pymysql.cursors.DictCursor)
    assert "This was to find" in dispatch(intent_req,db)




def parse_data(intent_request):
    slots_dictionary = {}
    #campaigns
    campaigns = []
    for key in intent_request['currentIntent']['slots'].keys():
        if "Campaign" in key:
            value = validate_ES_campaign(try_ex(lambda: intent_request['currentIntent']['slots'][key]))
            if value != None:
                campaigns.append(value)


    if len(campaigns) == 0:
        campaigns = None

    slots_dictionary["campaigns"] = campaigns

    #media
    media = []
    for key in intent_request['currentIntent']['slots'].keys():
        if "Media" in key:
            value = try_ex(lambda: intent_request['currentIntent']['slots'][key])
            if value != None:
                media.append(value)

    if len(media) == 0:
        media = None

    slots_dictionary["media"] = media
    #markets

    markets = []

    for key in intent_request['currentIntent']['slots'].keys():
        if "Market" in key:
            value = validate_ES_country(try_ex(lambda: intent_request['currentIntent']['slots'][key]))
            if value != None:
                markets.append(value)


    if len(markets) == 0:
        markets = None

    slots_dictionary["markets"] = markets

    dimensions = []

    for key in intent_request['currentIntent']['slots'].keys():
        if "Dimension" in key:
            value = try_ex(lambda: intent_request['currentIntent']['slots'][key])
            if value != None:
                dimensions.append(value)


    if len(dimensions) == 0:
        dimensions = None

    slots_dictionary["dimensions"] = dimensions

    #date parse
    date = try_ex(lambda: intent_request['currentIntent']['slots']['Date'])
    if date != None:
        date = parse_date(str(date),past=True)

    slots_dictionary['date'] = date

    #date parse
    date_comparison = try_ex(lambda: intent_request['currentIntent']['slots']['DateComparison'])
    if date_comparison != None:
        date = parse_date_comparison(str(date_comparison))

    slots_dictionary['DateComparison'] = date

    #metric
    slots_dictionary['Metric'] = try_ex(lambda: intent_request['currentIntent']['slots']['Metric'])

    #extreme
    slots_dictionary['Extreme'] = try_ex(lambda: intent_request['currentIntent']['slots']['Extreme'])

    return slots_dictionary

def test_comparison_addition():
    intent = {"currentIntent": {"name": "SingleMetric","slots": {"DateComparison":"week-on-week"}}}
    assert parse_data(intent)["DateComparison"] == {"group_a":{"start":"2018-02-18","end":"2018-02-24"},"group_b":{"start":"2018-02-11","end":"2018-02-17"}}


def validate_ES_campaign(campaign):
    if campaign is None:
        return None
    if "undesliga" in campaign:
        return "FootballBundesliga"
    if "asket" in campaign:
        return "Basket%"
    if "lympics" in campaign:
        return "WintersportsOlympics"
    if "quash" in campaign:
        return "SquashPsaworldtour"

def validate_ES_country(country):
    if country is None:
        return None
    if country in ['Germany','DE']:
        return 'DE'
    if country in ['NO','Norway']:
        return 'NO'
    if country in ['UK','GB','United Kingdom','Great Britain']:
        return 'UK'
    if country in ['IT','Italy']:
        return 'IT'
    if country in ['France','FR']:
        return 'FR'
    if country in ['NL','Netherlands']:
        return 'NL'
    if country in ['All','All Countries']:
        return 'SELECT DISTINCT(Countries) FROM prod_channels'
    if country in ['Nordics']:
        return "DK','NO','SE','FI"
    else:
        return country


def date_comparison_query(intent_request):

        slots_dictionary = parse_data(intent_request)

        group_a = "SELECT 'group_a' as segment,"

        if slots_dictionary['Metric'] == "SAC":

            group_a += "SUM(Spend)/SUM(Conversions) as SAC"

        elif slots_dictionary['Metric'] == "Subscriptions":

            group_a += "SUM(Conversions) as Subscriptions"

        elif slots_dictionary['Metric'] == "CVR":

            group_a += "SUM(Conversions)/SUM(Impressions)*100 as CVR"

        elif slots_dictionary['Metric'] == "CTR":

            group_a += "SUM(Clicks)/SUM(Impressions)*100 as CTR"

        elif slots_dictionary['Metric'] == "CPC":

            group_a += "SUM(Spend)/SUM(Clicks) as CTR"

        elif slots_dictionary['Metric'] == "Spend":

            group_a += "SUM(Spend) as Spend"

        dimensions = slots_dictionary['dimensions']

        if dimensions != None:
            for dimension in dimensions:
                group_a += " , " + dimension

        group_a += " FROM prod_channels"

        campaigns = slots_dictionary['campaigns']
        markets = slots_dictionary['markets']
        media = slots_dictionary['media']
        dates = slots_dictionary['DateComparison']

        if campaigns != None or markets != None or media != None or dates != None or slots_dictionary['Metric'] in ["SAC","Subscriptions"]:
            group_a += " WHERE"
            previous_where = False

            if media != None:
                if previous_where == True:
                    group_a += " AND"
                media_seq = media[0]
                for channel in media[1:]:
                    media_seq += "','" + channel
                group_a += " Channel in ('" + media_seq + "')"
                previous_where = True

            if campaigns != None:
                if previous_where == True:
                    group_a += " AND"
                campaign_seq = campaigns[0]
                for campaign in campaigns[1:]:
                    campaign_seq += "','" + campaign
                group_a += " Campaign in ('" + campaign_seq + "')"
                previous_where = True

            if markets != None:
                if previous_where == True:
                    group_a += " AND"
                market_seq = markets[0]
                for market in markets[1:]:
                    market_seq += "','" + market
                group_a += " Country in ('" + market_seq + "')"
                previous_where = True

            if dates != None:
                if previous_where == True:
                    group_a += " AND"
                group_a += " Date >= '" + dates['group_a']['start'] + "' and Date <= '" + dates['group_a']['end'] + "'"
                previous_where = True


            if slots_dictionary['Metric'] == "SAC":
                if previous_where == True:
                    group_a += " AND"
                group_a += " Spend > 0 and Conversions > 0"
                previous_where = True

            if slots_dictionary['Metric'] == "Subscriptions":
                if previous_where == True:
                    group_a += " AND"
                group_a += " Conversions > 0"
                previous_where = True

        group_b = "SELECT 'group_b' as segment,"

        if slots_dictionary['Metric'] == "SAC":

            group_b += "SUM(Spend)/SUM(Conversions) as SAC"

        elif slots_dictionary['Metric'] == "Subscriptions":

            group_b += "SUM(Conversions) as Subscriptions"

        elif slots_dictionary['Metric'] == "CVR":

            group_b += "SUM(Conversions)/SUM(Impressions)*100 as CVR"

        elif slots_dictionary['Metric'] == "CTR":

            group_b += "SUM(Clicks)/SUM(Impressions)*100 as CTR"

        elif slots_dictionary['Metric'] == "CPC":

            group_b += "SUM(Spend)/SUM(Clicks) as CTR"

        elif slots_dictionary['Metric'] == "Spend":

            group_b += "SUM(Spend) as Spend"

        dimensions = slots_dictionary['dimensions']

        if dimensions != None:
            for dimension in dimensions:
                group_b += " , " + dimension

        group_b += " FROM prod_channels"

        campaigns = slots_dictionary['campaigns']
        markets = slots_dictionary['markets']
        media = slots_dictionary['media']
        dates = slots_dictionary['DateComparison']

        if campaigns != None or markets != None or media != None or dates != None or slots_dictionary['Metric'] in ["SAC","Subscriptions"]:
            group_b += " WHERE"
            previous_where = False

            if media != None:
                if previous_where == True:
                    group_b += " AND"
                media_seq = media[0]
                for channel in media[1:]:
                    media_seq += "','" + channel
                group_b += " Channel in ('" + media_seq + "')"
                previous_where = True

            if campaigns != None:
                if previous_where == True:
                    group_b += " AND"
                campaign_seq = campaigns[0]
                for campaign in campaigns[1:]:
                    campaign_seq += "','" + campaign
                group_b += " Campaign in ('" + campaign_seq + "')"
                previous_where = True

            if markets != None:
                if previous_where == True:
                    group_b += " AND"
                market_seq = markets[0]
                for market in markets[1:]:
                    market_seq += "','" + market
                group_b += " Country in ('" + market_seq + "')"
                previous_where = True

            if dates != None:
                if previous_where == True:
                    group_b += " AND"
                group_b += " Date >= '" + dates['group_b']['start'] + "' and Date <= '" + dates['group_b']['end'] + "'"
                previous_where = True


            if slots_dictionary['Metric'] == "SAC":
                if previous_where == True:
                    group_b += " AND"
                group_b += " Spend > 0 and Conversions > 0"
                previous_where = True

            if slots_dictionary['Metric'] == "Subscriptions":
                if previous_where == True:
                    group_b += " AND"
                group_b += " Conversions > 0"
                previous_where = True


        query = "SELECT * FROM (%s UNION %s) unioned GROUP BY segment;" % (group_a,group_b)
        print query
        return query

def test_comparitive_performance_query():
    assert date_comparison_query({"currentIntent": {"name": "FindBest","slots": {"Metric":"SAC","DateComparison":{"group_a":{"start":"2018-02-18","end":"2018-02-24"},"group_b":{"start":"2018-02-11","end":"2018-02-17"}}}}}) == "SELECT * FROM (SELECT 'group_a' as segment,SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Spend > 0 and Conversions > 0 UNION SELECT 'group_b' as segment,SUM(Spend)/SUM(Conversions) as SAC FROM prod_channels WHERE Spend > 0 and Conversions > 0) unioned GROUP BY segment;"

def prod_channels_query(query,campaigns,markets,date,media,dimensions):

    #define the group by
    if dimensions != None:
        for dimension in dimensions:
            query += " , " + dimension


    query += " FROM prod_channels"
    #define the where
    if campaigns != None or markets != None or media != None or date != None:
        query += " WHERE"
        previous_where = False

        if media != None:
            if previous_where == True:
                query += " AND"
            media_seq = media[0]
            for channel in media[1:]:
                media_seq += "','" + channel
            query += " Channel in ('" + media_seq + "')"
            previous_where = True

        if campaigns != None:
            if previous_where == True:
                query += " AND"
            campaign_seq = campaigns[0]
            for campaign in campaigns[1:]:
                campaign_seq += "','" + campaign
            query += " Campaign in ('" + campaign_seq + "')"
            previous_where = True

        if markets != None:
            if previous_where == True:
                query += " AND"
            market_seq = markets[0]
            for market in markets[1:]:
                market_seq += "','" + market
            query += " Country in ('" + market_seq + "')"
            previous_where = True

        if date != None:
            if previous_where == True:
                query += " AND"
            query += " Date >= '" + date['start'] + "' and Date <= '" + date['end'] + "'"
            previous_where = True

    #define the group by
    if dimensions != None:
        query += " GROUP BY "
        previous_groupby = False
        for dimension in dimensions:
            if previous_groupby == False:
                query += dimension
            else:
                query += " , " + dimension

            previous_groupby = True

    query += ";"

    return query


def generate_metric_query(metric,intent_request):
    if metric == "Spend":
        #preprocess the request data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Spend) as Spend"
        query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query
    elif metric == "Subscriptions":
        #proproces the requrest data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Conversions) as Subscriptions"
        query = query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query
    elif metric == "SAC":
        #process the request data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Spend)/sum(Conversions) as SAC"
        query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query
    elif metric == "CTR":
        #proproces the requrest data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Clicks)/sum(Impressions)*100 as CTR"
        query =  prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query

    elif metric == "CVR":
        #proproces the requrest data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Conversions)/sum(Clicks)*100 as CVR"
        query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query

    elif metric == "CPC":
        #proproces the requrest data
        slots_dictionary = parse_data(intent_request)
        #define the columns
        query = "SELECT sum(Spend)/sum(Clicks)*100 as CPC"
        query = prod_channels_query(query=query,campaigns=slots_dictionary["campaigns"],markets=slots_dictionary["markets"],date=slots_dictionary["date"],media=slots_dictionary["media"],dimensions=slots_dictionary["dimensions"])
        print "query: ",query
        return query

def comparison_query(intent_request):

    slots_dictionary = parse_data(intent_request)

    if slots_dictionary['Metric'] == "SAC":

        query = "SELECT SUM(Spend)/SUM(Conversions) as SAC"

    elif slots_dictionary['Metric'] == "Subscriptions":

        query = "SELECT SUM(Conversions) as Subscriptions"

    elif slots_dictionary['Metric'] == "CVR":

        query = "SELECT SUM(Conversions)/SUM(Impressions)*100 as CVR"

    elif slots_dictionary['Metric'] == "CTR":

        query = "SELECT SUM(Clicks)/SUM(Impressions)*100 as CTR"

    elif slots_dictionary['Metric'] == "CPC":

        query = "SELECT SUM(Spend)/SUM(Clicks) as CTR"

    elif slots_dictionary['Metric'] == "Spend":

        query += "SUM(Spend) as Spend"

    dimensions = slots_dictionary['dimensions']

    if dimensions != None:
        for dimension in dimensions:
            query += " , " + dimension

    query += " FROM prod_channels"

    campaigns = slots_dictionary['campaigns']
    markets = slots_dictionary['markets']
    media = slots_dictionary['media']
    date = slots_dictionary['date']

    if campaigns != None or markets != None or media != None or date != None or slots_dictionary['Metric'] in ["SAC","Subscriptions"]:
        query += " WHERE"
        previous_where = False

        if media != None:
            if previous_where == True:
                query += " AND"
            media_seq = media[0]
            for channel in media[1:]:
                media_seq += "','" + channel
            query += " Channel in ('" + media_seq + "')"
            previous_where = True

        if campaigns != None:
            if previous_where == True:
                query += " AND"
            campaign_seq = campaigns[0]
            for campaign in campaigns[1:]:
                campaign_seq += "','" + campaign
            query += " Campaign in ('" + campaign_seq + "')"
            previous_where = True

        if markets != None:
            if previous_where == True:
                query += " AND"
            market_seq = markets[0]
            for market in markets[1:]:
                market_seq += "','" + market
            query += " Country in ('" + market_seq + "')"
            previous_where = True

        if date != None:
            if previous_where == True:
                query += " AND"
            query += " Date >= '" + date['start'] + "' and Date <= '" + date['end'] + "'"
            previous_where = True


        if slots_dictionary['Metric'] == "SAC":
            if previous_where == True:
                query += " AND"
            query += " Spend > 0 and Conversions > 0"
            previous_where = True

        if slots_dictionary['Metric'] == "Subscriptions":
            if previous_where == True:
                query += " AND"
            query += " Conversions > 0"
            previous_where = True


    if dimensions != None:
        query += " GROUP BY "
        previous_groupby = False
        for dimension in dimensions:
            if previous_groupby == False:
                query += dimension
            else:
                query += " , " + dimension

    if slots_dictionary['Extreme'] == "best":
        if slots_dictionary['Metric'] == "SAC":

            query += " ORDER BY SAC ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "Subscriptions":

            query += " ORDER BY Subscriptions DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CVR":

            query += " ORDER BY CVR DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CTR":

            query += " ORDER BY CTR DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CPC":

            query += " ORDER BY CPC ASC LIMIT 1;"

        else:
            raise ValueError("Unable to form query")

    elif slots_dictionary['Extreme'] == "worst":
        if slots_dictionary['Metric'] == "SAC":

            query += " ORDER BY SAC DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "Subscriptions":

            query += " ORDER BY Subscriptions ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CVR":

            query += " ORDER BY CVR ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CTR":

            query += " ORDER BY CTR ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CPC":

            query += " ORDER BY CPC DESC LIMIT 1;"

        else:
            raise ValueError("Unable to form query")

    elif slots_dictionary['Extreme'] == "most":
        if slots_dictionary['Metric'] == "SAC":

            query += " ORDER BY SAC DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "Subscriptions":

            query += " ORDER BY Subscriptions DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CVR":

            query += " ORDER BY CVR DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CTR":

            query += " ORDER BY CTR DESC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CPC":

            query += " ORDER BY CPC DESC LIMIT 1;"

        else:
            raise ValueError("Unable to form query")

    elif slots_dictionary['Extreme'] == "least":
        if slots_dictionary['Metric'] == "SAC":

            query += " ORDER BY SAC ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "Subscriptions":

            query += " ORDER BY Subscriptions ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CVR":

            query += " ORDER BY CVR ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CTR":

            query += " ORDER BY CTR ASC LIMIT 1;"

        elif slots_dictionary['Metric'] == "CPC":

            query += " ORDER BY CPC ASC LIMIT 1;"

        else:
            raise ValueError("Unable to form query")


    print query
    return query
